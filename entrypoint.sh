#!/bin/bash

set -e

# Start the php-fpm service in the background
php-fpm &

# Start the nginx service in the foreground
nginx -g 'daemon off;'
