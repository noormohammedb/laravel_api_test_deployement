# FROM nginx:latest

# COPY . /var/www/html/
# COPY nginx.conf /etc/nginx/conf.d/default.conf

# # dev tools to debug
# RUN apt-get update && apt-get install -y vim  htop net-tools tmux git zsh

# RUN apt-get update && apt-get install -y \
#     curl \
#     libpng-dev \
#     libonig-dev \
#     libxml2-dev \
#     zip \
#     unzip \
#     && docker-php-ext-install pdo_mysql \
#     && docker-php-ext-install mbstring \
#     && docker-php-ext-install exif \
#     && docker-php-ext-install pcntl \
#     && docker-php-ext-install bcmath \
#     && docker-php-ext-install gd \
#     && docker-php-ext-install opcache

# FROM php:7.4-fpm


# RUN docker-php-ext-install pdo_mysql \
# && docker-php-ext-install mbstring \
# && docker-php-ext-install exif \
# && docker-php-ext-install pcntl \
# && docker-php-ext-install bcmath \
# && docker-php-ext-install gd \
# && docker-php-ext-install opcache


# COPY nginx.conf /etc/nginx/nginx.conf
# COPY nginx.conf /etc/nginx/conf.d/default.conf

# COPY .env.docker .env

# RUN chown -R www-data:www-data storage bootstrap/cache

FROM php:8.2.6RC1-fpm

RUN apt-get update && apt-get install -y vim htop net-tools tmux git zsh

RUN apt-get update && apt-get install -y \
    nginx \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip

WORKDIR /var/www/html
RUN docker-php-ext-install pdo_mysql

COPY . .
COPY nginx.conf /etc/nginx/sites-enabled/default


RUN php artisan key:generate
RUN php artisan config:cache
RUN php artisan route:cache

COPY entrypoint.sh /usr/local/bin/

RUN chown -R www-data:www-data storage bootstrap/cache

RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

